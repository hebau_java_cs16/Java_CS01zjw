
import java.util.*;
public class Ping {

    public static void main(String[] args) {
    System.out.println("请评委给5位选手在1-10分之内打分！\n");
    int i,j;
    int[][] arr={{9,6,8,8,7,5,6,9,7,8},{9,8,7,9,8,7,9,8,8,5},{6,5,4,9,8,7,8,6,9,7},{8,5,9,6,7,9,6,8,7,8},{9,8,5,6,7,9,8,7,5,6}};
    double[] ave = new double[5];
    System.out.println("5位选手成绩分别为：");
    for(i=0;i<5;i++){
        for(j=0;j<10;j++){
            System.out.printf("%5d",arr[i][j]);
        }
        System.out.printf("\n");
    }
    int[] max=maxscore(arr);
    System.out.println("5位选手最高成绩分别为：");
    for(i=0;i<max.length;i++)
    {
        System.out.printf("%5d",max[i]);
    }
    int[] min=minscore(arr);
    System.out.println("\n5位选手最低成绩分别为：");
    for(i=0;i<min.length;i++)
    {
        System.out.printf("%5d",min[i]);
    }
    int[] sum=sumscore(arr);
    System.out.println("\n5位选手去掉最高分最低分的总成绩分别为：");
    for(i=0;i<sum.length;i++)
    {
        System.out.printf("%5d",sum[i]);
    }
    float[] aver=ave(sum);
    System.out.println("\n5位选手平均成绩分别为：");
    for(i=0;i<aver.length;i++)
    {
        System.out.printf("%5.1f  ",aver[i]);
    }
    
    }
    public static int[] maxscore(int arr[][]){      //最大
        int i,j;
        int[] brr=new int[5];
        for(i=0;i<5;i++){
            brr[i]=arr[i][0];
            for(j=0;j<10;j++){
                     if(brr[i]<arr[i][j])
                      brr[i]=arr[i][j];
            }
        }
        return brr;
    }
    public static int[] minscore(int arr[][]){     //最小
        int i,j;
        int[] crr=new int[5];
        for(i=0;i<5;i++){
            crr[i]=arr[i][0];
            for(j=0;j<10;j++){
                 if(crr[i]>arr[i][j])
                    crr[i]=arr[i][j];
            }
        }
        return crr;
    }
    public static int[] sumscore(int arr[][]){      //求和
        int i,j;
        int[] sum=new int[5];
        int[] max=maxscore(arr);
        int[] min=minscore(arr);
        for(i=0;i<5;i++){
            for(j=0;j<10;j++) {
                sum[i]+=arr[i][j];
            }
                sum[i]-=(max[i]+min[i]);
        }
        return sum;
    }
    public static float[] ave(int sum[]) {      //平均
        int i;
        float[] ave=new float[5];
        for(i=0;i<sum.length;i++) {
            ave[i]=(float) (sum[i]/3.0);
        }
        return ave;
    }
}