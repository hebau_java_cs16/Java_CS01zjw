package wanninali;

import java.util.Scanner;
public class wanninali {

	public static void main(String[] args){
		Scanner in=new Scanner (System.in);
		int year=in.nextInt();
		int month=in.nextInt();
		 printCalender(year, month);
	}
	public static boolean isLeap(int year){//判断是否为闰年 是闰年返回true
		if((year%4==0&&year%100!=0)||year%400==0){
			return true;
		}
		else{
			return false;
		}
	}
	public static int days(int year, int month){//求一年中截止到某月的天数  返回天数总和
		int sum=0;
		for(int i=1;i<month;i++){
			if(i==1||i==3||i==5||i==7||i==8||i==10||i==12){
				sum=sum+31;
			}
			else if(i==4||i==6||i==9||i==11){
				sum=sum+30;
			}
			else if(i==2){
				if((isLeap(year)==true)){
					sum=sum+29;
				}
				else{
					sum=sum+28;
				}
			}
		}
		return sum;
	}
	public static int totalDays(int year, int month){
		int sum=0,sum1=0;	
		for(int i=year-1;i>=1900;i--){
			
			if(isLeap(i)==true){
				sum=sum+366;
			}
			else{
				sum=sum+365;
			}
		}
		sum1=sum+days(year,month);
		return sum1;
	}
	public static int dayMonth(int year,int month){
		int daymonth = 0;
		if(month==1||month==3||month==5||month==7||month==8||month==10||month==12){
			daymonth=31;
		}
		else if(month==4||month==6||month==9||month==11){
			daymonth=30;
		}
		else if(month==2){
			if(isLeap(year)==true){
				daymonth=29;
			}
			else{
				daymonth=28;
			}
		}
		return daymonth;
	}
	public static void printCalender(int year, int month){
		System.out.println("星期日\t星期一\t星期二\t星期三\t星期四\t星期五\t星期六\t");
		int day;           
		int daymonth=dayMonth(year,month);
		//day=(1+totalDays(year,month)%7)%7;
		day=(1+totalDays(year,month))%7;
		for(int i=0;i<day;i++){
			System.out.printf("\t");			
		}
		for(int i=1;i<=daymonth;i++){
				System.out.printf("%d\t",i);
				//flag++;
				if((i+day)%7==0){      
					System.out.printf("\n");
				}
			}
	}
}

